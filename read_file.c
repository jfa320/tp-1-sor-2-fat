#include <stdio.h>
#include <stdlib.h>

int main(int argc, char * argv[])
{
    if(argc!=2){
        printf("Error: ingresé el nombre de archivo");
        return 1;
    }

    printf("Muestra contenido de %s\n\n", argv[1]);
    FILE *in;
    char con[1000];
    in= fopen(argv[1], "r");

    if(!in) {
        printf("Error al intentar abrir %s", argv[1]);
        return 1;
    }

    while (fgets(con, 1000, in) != NULL )
    {
        printf( "%s\n", con);
    }

   fclose(in);
    return 0;
}
