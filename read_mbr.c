#include <stdio.h>
#include <stdlib.h>

int main() {
    FILE * in = fopen("test.img", "rb");
    unsigned int i, start_sector, length_sectors;
    
    fseek(in, 446 , SEEK_SET); // Voy al inicio de las entradas a la tabla de particiones
    /*
    for(i=0; i<4; i++) { // Leo las entradas | notar que leo las 4 entradas pero en este caso solo uso 1
        printf("Partition entry %d: First byte (bootable flag) %02X\n", i, fgetc(in));
        printf("  Comienzo de partición en CHS: %02X:%02X:%02X\n", fgetc(in), fgetc(in), fgetc(in));
        printf("  Partition type 0x%02X\n", fgetc(in));
        printf("  Fin de partición en CHS: %02X:%02X:%02X\n", fgetc(in), fgetc(in), fgetc(in));
        
        fread(&start_sector, 4, 1, in);
        fread(&length_sectors, 4, 1, in);
        printf("  Dirección LBA relativa 0x%08X, de tamaño en sectores %d\n", start_sector, length_sectors);
    }
    */
    //lo saco del ciclo ya que el enunciado me pide solo para la primer particion no para las 4
    printf("Partition entry %d: First byte (bootable flag) %02X\n", i, fgetc(in));
    printf("  Comienzo de partición en CHS: %02X:%02X:%02X\n", fgetc(in), fgetc(in), fgetc(in));
    printf("  Partition type 0x%02X\n", fgetc(in));
    printf("  Fin de partición en CHS: %02X:%02X:%02X\n", fgetc(in), fgetc(in), fgetc(in));
        
    fread(&start_sector, 4, 1, in);
    fread(&length_sectors, 4, 1, in);
    printf("  Dirección LBA relativa 0x%08X, de tamaño en sectores %d\n", start_sector, length_sectors);
    fclose(in);
    return 0;
}
