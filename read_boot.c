#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    unsigned char first_byte;
    unsigned char start_chs[3];
    unsigned char partition_type;
    unsigned char end_chs[3];
    char start_sector[4];
    char length_sectors[4];
} __attribute((packed)) PartitionTable; //sirve para leer datos de la particion

typedef struct
{
    unsigned char jmp[3];
    char oem[8];
    unsigned short sector_size; // 2 bytes

    unsigned char sectorsPerCluster;                  //1 byte
    unsigned short sizeReservedArea;                  //2 bytes  | en sectores
    unsigned char numberOfFats;                       //1 byte
    unsigned short maxFilesInRootDirectory;           //2bytes
    unsigned short numberOfSectorsFS;                 //2 bytes | si no es suficiente se deberá usar 4B en bytes 32 - 35 (numberOfSectorsFSExtended)
    unsigned char mediaType;                          //1 byte
    unsigned short sizeOfEachFat;                     //2 bytes
    unsigned short sectorsPerTrack;                   //2 bytes
    unsigned short numbersOfHead;                     //2 bytes
    unsigned int numberOfSectorsBeforeStartPartition; //4 bytes
    unsigned int numberOfSectorsFSExtended;           //4bytes (ver numberOfSectorsFS)
    unsigned char bios13h;                            //1 byte
    char notUsed;                                     //1 byte
    char bootSignature;                               //1byte
    char volume_id[4];
    char volume_label[11];
    char fs_type[8]; // Type en ascii
    char boot_code[448];
    unsigned short boot_sector_signature;
} __attribute((packed)) Fat12BootSector; //para leer los datos del boot sector

int main()
{

    //abro el archivo img
    FILE *in = fopen("test.img", "rb");
    //contador para usar en el ciclo
    int i;
    //creo una instancia de Partition Table
    PartitionTable pt[4];
    //creo una instancia de Fat12BootSector
    Fat12BootSector bs;

    fseek(in, 446, SEEK_SET);                 // Ir al inicio de la tabla de particiones. | 446 esta muy hardcodeado,preguntar
    fread(pt, sizeof(PartitionTable), 4, in); // leo entradas y las guardo en la instancia de partition table creada antes

    for (i = 0; i < 4; i++)
    {
        printf("Partition type: %d\n", pt[i].partition_type);
        if (pt[i].partition_type == 1)
        {
            printf("Encontrado FAT12 %d\n", i);
            break;
        }
    }
    if (i == 4)
    {
        printf("No se encontró filesystem FAT12, saliendo ...\n");
        return -1;
    }
    fseek(in, 0, SEEK_SET);                     //vuelvo al inicio de la imagen
    fread(&bs, sizeof(Fat12BootSector), 1, in); //leo el boot sector de la imagen y se lo asigno

    //imprimo todos los datos del boot sector
    printf("  Jump code: %02X:%02X:%02X\n", bs.jmp[0], bs.jmp[1], bs.jmp[2]);
    printf("  OEM code: [%.8s]\n", bs.oem);
    printf("  sector_size: %d\n", bs.sector_size);
    printf("  Sectors per cluster: %i\n", bs.sectorsPerCluster);
    printf("  Size of reserved area, in sectors: %i\n", bs.sizeReservedArea);
    printf("  Number Of Fats: %i\n", bs.numberOfFats);
    printf("  Maximum number of files in the root directory : %i\n", bs.maxFilesInRootDirectory);
    printf("  Number of sectors in the file system: %i\n", bs.numberOfSectorsFS);
    printf("  Media Type (0xF0=removable disk, 0xF8=fixed disk): 0x%02X\n", bs.mediaType);
    printf("  Size of each FAT, in sectors: %i\n", bs.sizeOfEachFat);
    printf("  Sectors per track in storage device : %i\n", bs.sectorsPerTrack);
    printf("  Numbers of Head: %i\n", bs.numbersOfHead);
    printf("  Number Of Sectors Before Start Partition: %u\n", bs.numberOfSectorsBeforeStartPartition);
    printf("  Number Of Sectors in FS (Extended - 0 if not used): %i \n", bs.numberOfSectorsFSExtended);
    printf("  BIOS INT 13h (low level disk services) drive number: %02X \n", bs.bios13h);
    printf("  Extended boot signature: 0x%02X \n", bs.bootSignature);

    printf("  volume_id: %s\n", bs.volume_id);
    printf("  Volume label: [%.11s]\n", bs.volume_label);
    printf("  Filesystem type: [%.8s]\n", bs.fs_type);
    printf("  Boot sector signature: 0x%04X\n", bs.boot_sector_signature);

    //cierro el archivo
    fclose(in);
    return 0;
}
