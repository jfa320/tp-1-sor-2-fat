#include <stdio.h>
#include <stdlib.h>

typedef struct
{
    unsigned char first_byte;
    unsigned char start_chs[3];
    unsigned char partition_type;
    unsigned char end_chs[3];
    char starting_cluster[4];
    char file_size[4];
} __attribute((packed)) PartitionTable;

typedef struct
{
    unsigned char jmp[3];
    char oem[8];
    unsigned short sector_size; // 2 bytes

    unsigned char sectorsPerCluster;                  //1 byte
    unsigned short sizeReservedArea;                  //2 bytes  | en sectores
    unsigned char numberOfFats;                       //1 byte
    unsigned short maxFilesInRootDirectory;           //2bytes
    unsigned short numberOfSectorsFS;                 //2 bytes | si no es suficiente se deberá usar 4B en bytes 32 - 35 (numberOfSectorsFSExtended)
    unsigned char mediaType;                          //1 byte
    unsigned short sizeOfEachFat;                     //2 bytes
    unsigned short sectorsPerTrack;                   //2 bytes
    unsigned short numbersOfHead;                     //2 bytes
    unsigned int numberOfSectorsBeforeStartPartition; //4 bytes
    unsigned int numberOfSectorsFSExtended;           //4bytes (ver numberOfSectorsFS)
    unsigned char bios13h;                            //1 byte
    char notUsed;                                     //1 byte
    char bootSignature;                               //1byte
    char volume_id[4];
    char volume_label[11];
    char fs_type[8]; // Type en ascii
    char boot_code[448];
    unsigned short boot_sector_signature;

} __attribute((packed)) Fat12BootSector;

typedef struct
{
    unsigned char filename[8];         //8
    unsigned char extension[3];        //11
    unsigned char attributes;          //12
    unsigned char reserved;            //13
    unsigned char create_time_ms[3];   //16
    unsigned char create_date[2];      //18
    unsigned char last_access_date[2]; //20
    unsigned short first_cluster_msb;  //22
    unsigned char last_mod_time[2];    //24
    unsigned char last_mod_date[2];    //26
    unsigned short first_cluster_lsb;  //28
    unsigned int file_size;            //32
}
__attribute((packed)) Fat12Entry;

void print_file_info(Fat12Entry *entry)
{
    switch (entry->filename[0])
    {
    case 0x00:
        return;                                                                       // unused entry
    case 0x05:                                                                        
        printf("Archivo borrado: [?%.7s.%.3s]\n", entry->filename, entry->extension); 
        return;
    case 0xE5:        
        printf("Archivo que comienza con 0xE5: [%c%.7s.%.3s]\n", 0xE5, entry->filename, entry->extension); 
        break;
  
    default:
        if(entry->attributes == 0x10){
           printf("Directorio: [%.8s]\n", entry->filename); 
           break; 
        }else{
            if(entry->attributes == 0x20){
                printf("Archivo: [%.8s.%.3s]\n", entry->filename, entry->extension);
            }
        }
    }
}

int main()
{
    FILE *in = fopen("test.img", "rb");
    int i;
    PartitionTable pt[4];
    Fat12BootSector bs;
    Fat12Entry entry;

     fseek(in, 446 , SEEK_SET);
     fread(pt, sizeof(PartitionTable), 4, in);  

    for (i = 0; i < 4; i++)
    {
        if (pt[i].partition_type == 1)
        {
            printf("Encontrada particion FAT12 %d\n", i);
            break;
        }
    }

    if (i == 4)
    {
        printf("No encontrado filesystem FAT12, saliendo...\n");
        return -1;
    }

    fseek(in, 0, SEEK_SET);
    fread(&bs, sizeof(Fat12BootSector), 1, in); 

    printf("En  0x%X, sector size %d, FAT size %d sectors, %d FATs\n\n",
           ftell(in), bs.sector_size, bs.sizeOfEachFat, bs.numberOfFats);

    fseek(in, (bs.sizeReservedArea - 1 + bs.sizeOfEachFat * bs.numberOfFats) * bs.sector_size, SEEK_CUR);

    printf("Root dir_entries %d \n", bs.maxFilesInRootDirectory);
    for (i = 0; i < bs.maxFilesInRootDirectory; i++)
    {
        fread(&entry, sizeof(entry), 1, in);
        print_file_info(&entry);
    }

    printf("\nLeido Root directory, ahora en 0x%X\n", ftell(in));
    fclose(in);
    return 0;
}
